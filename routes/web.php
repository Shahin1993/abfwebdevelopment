<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/','FrontendController@View');
Route::get('/blogAdmin','Admin\AdminController@dashboard');

Route::get('/signUp','RegistrationController@create');
Route::post('/signUp','RegistrationController@register');
Route::get('/loginUser','LoginController@loginForm');
Route::post('/loginUser','LoginController@login');
Route::get('/logout','LoginController@logout');


//department

// Route::get('/departments/create','DepartmentController@create');

// Route::post('/departments','DepartmentController@store');


// Route::get('/departments','DepartmentController@index');
// // Route::get('/department-delete/{id}','DepartmentController@destroy');
// Route::delete('/departments/{id}','DepartmentController@destroy');
// Route::get('/departments/{id}/edit','DepartmentController@edit');


// Route::patch('/departments/{id}','DepartmentController@update');


Route::resource('departments','DepartmentController');




//Student


Route::resource('/students','StudentController');

// Route::get('/students/create','StudentController@create');
// Route::post('/students','StudentController@store');

Route::get('/admissions/create','AdmissionController@create');

Route::post('/admissions','AdmissionController@store');
Route::get('/admissions','AdmissionController@index');

Route::get('/admissions/{id}/edit','AdmissionController@edit');
Route::patch('/admissions/{id}','AdmissionController@update');
Route::delete('/admissions/{id}','AdmissionController@destroy');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 Route::resource('categories','Admin\CategoryController');
 Route::resource('posts','Admin\PostController');
 Route::get('/pdf','AdmissionController@pdf');

 Route::get('/blogs','Frontend\BlogController@view');
 Route::get('/blogDetails/{id}','Frontend\BlogController@details');

 Route::post('/comment','Frontend\CommentController@store');