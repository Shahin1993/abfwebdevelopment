@extends('layouts.frontend.master')
@section('title','Student update')

@section('content')

<br>
<h2>Update </h2>
@include('messages.message')

	 	{!! Form::open(['url' => '/students/'.$data->id, 'method'=>'PATCH', 'class'=>'form-group','enctype'=>'multipart/form-data']) !!}

	 

		<input type="text" name="std_name" placeholder="student name" value="{{$data->std_name}}">
		<input type="text" name="std_id" placeholder="std id" value="{{$data->std_id}}">
		<input type="text" name="mobile" placeholder="mobile" value="{{$data->mobile}}">
		<input type="email" name="email" placeholder="email" value="{{$data->email}}">

		<input type="file" name="image" >
		 <img src=" {{asset('images/'.$data->image)}} " height="100" />


		<input type="submit" value="Save and Change">
		
   {{Form::close()}}

@endsection