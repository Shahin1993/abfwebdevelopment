@extends('layouts.frontend.master')
@section('title','Student view')

@section('content')

<br>

<h2> <a href="/students/create">Add student</a> </h2>@include('messages.message')
<table border="1">
	
<tr>
	<td>Name</td>
	<td>Photo</td>
	<td>Edit</td>
	<td>Delete</td>
</tr>
@foreach($students as $data)
<tr>
	<td>{{$data->std_name}}</td>
	<td>  <img src=" {{asset('images/'.$data->image)}} " height="100" /> </td>
	<td> <a href="/students/{{$data->id}}/edit">Edit</a> </td>
	<td> {!! Form::open(['url' => '/students/'.$data->id, 'method'=>'Delete']) !!}

			<button type="submit" class="btn btn-warning"  onclick="return confirm('are you sure?')">Delete</button>
   
            {!! Form::close() !!} </td>
</tr>
@endforeach

</table>


@endsection