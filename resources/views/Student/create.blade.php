<!DOCTYPE html>
<html>
<head>
	<title>Student Create</title>
</head>
<body>

	<h2>Add Student</h2>
	<h2><a href="/students"> view</a></h2>
	
  @include('messages.message')

	<form action="/students" method="POST" enctype="multipart/form-data">

		{{csrf_field()}}

		<input type="text" name="std_name" placeholder="student name">
		<input type="text" name="std_id" placeholder="std id">
		<input type="text" name="mobile" placeholder="mobile">
		<input type="email" name="email" placeholder="email">
		<input type="file" name="image" >

		<input type="submit" value="Submit">
		
	</form>

</body>
</html>