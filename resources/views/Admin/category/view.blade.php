@extends('layouts.frontend.master')
@section('title','Category Add')

@section('content')



<div class="container">
	@include('messages.message')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
  <h2>Category add form</h2>
  <form action="/categories" method="POST">
  	{{csrf_field()}}
  	<div class="form-group">
      <label for="fname">Category name:</label>
      <input type="text" class="form-control" id="fname" placeholder="Category" name="category">
    </div>
    
    	<div class="form-group">
      <label for="fname">Category Code:</label>
      <input type="text" class="form-control" id="fname" placeholder="code" name="cat_code">
    </div>
 
    <button type="submit" class="btn btn-default">Submit</button>
  </form>

</div></div><!--wendrow-->

<br>
<div class="row">

<div class="col-md-12">
	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>SL</th>
				<th>Category name</th>
				<th>Category Code</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			@foreach($category as $key=>$data)
			<tr>
				<td>{{++$key}}</td>
				<td>{{$data->category}}</td>
				<td>{{$data->cat_code}}</td>
				<td><a href="/categories/{{$data->id}}/edit">Edit</a></td>
				
				<td>	{!! Form::open(['url' => '/categories/'.$data->id, 'method'=>'Delete']) !!}

			<button type="submit" class="btn btn-warning"  onclick="return confirm('are you sure?')">Delete</button>
   
            {!! Form::close() !!}</td>
			</tr>
			 @endforeach
		</tbody>
	</table>
</div>

</div><!--endrow-->
</div>


<br>


@endsection