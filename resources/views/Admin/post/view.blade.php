@extends('layouts.frontend.master')
@section('title','POST Add')

@section('content')



<div class="container">
	@include('messages.message')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
  <h2>Post Now</h2>
  <form action="/posts" method="POST" enctype="multipart/form-data">
  	{{csrf_field()}}

  	<div class="form-group">
      <label for="fname">Select a Category</label>
      <select name="categories_id" class="form-control">
      	<option value="">Select a Category</option>
         @foreach($category as $cat)
         <option value="{{$cat->id}}">{{$cat->category}}</option>

         @endforeach
      </select>
     
    </div>
    

  		<div class="form-group">
      <label for="fname">Upload Photo</label>
      <input type="file" class="form-control" id="fname" placeholder="Category" name="image">
    </div>
  	<div class="form-group">
      <label for="fname">Title</label>
      <input type="text" class="form-control" id="fname" placeholder="title" name="title">
    </div>
    
    	<div class="form-group">
      <label for="fname">Description</label>
      <textarea name="description" class="form-control" id="summernote"></textarea>
     
    </div>
 
    <button type="submit" class="btn btn-default">Post</button>
  </form>

</div></div><!--wendrow-->

<br>
<div class="row">

<div class="col-md-12">
	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>SL</th>
				<th>Category name</th>
				<th>Title</th>
				<th>Description</th>
				<th>Image</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			@foreach($posts as $key=>$data)
			<tr>
				<td>{{++$key}}</td>
				<td>{{$data->categories->category}}</td>
				<td>{{$data->title}}</td>
				<td>{{substr($data->description,0,60)}} ...</td>
				<td> <img src="{{asset('images/'.$data->image)}}" width="100" > </td>
				<td><a href="/categories/{{$data->id}}/edit">Edit</a></td>
				
				<td>	{!! Form::open(['url' => '/categories/'.$data->id, 'method'=>'Delete']) !!}

			<button type="submit" class="btn btn-warning"  onclick="return confirm('are you sure?')">Delete</button>
   
            {!! Form::close() !!}</td>
			</tr>
			 @endforeach
		</tbody>
	</table>
</div>

</div><!--endrow-->
</div>


<br>
@endsection
@section('js')
 <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  $('#summernote').summernote({
   	height:'100px',
	placeholder:'Description',
	 
  });
  
});
	
</script>

@endsection