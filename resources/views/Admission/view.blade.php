@extends('layouts.frontend.master')

@section('title','View')

@section('content')

<br><br>
@include('messages.message')
<table border="1">
	
	<tr>
		<th>Sl</th>
		<th>Department</th>
		<th>Std Name</th>
		<th>mobile</th>
		<th>address</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>

    @foreach($admission as  $key=>$data)
	<tr>

		<td> {{++$key}}  </td>
		<td> {{$data->dpt_name}} </td>
		<td> {{$data->std_name}} </td>
		<td> {{$data->mobile}} </td>
		<td> {{$data->address}} </td>

		<td> <a href="/admissions/{{$data->id}}/edit">Edit</a> </td>
		<td>
				{!! Form::open(['url' => '/admissions/'.$data->id, 'method'=>'Delete']) !!}

			<button type="submit" class="btn btn-warning"  onclick="return confirm('are you sure?')">Delete</button>
   
            {!! Form::close() !!}
		</td>
		
	</tr>

	@endforeach


</table>


@endsection