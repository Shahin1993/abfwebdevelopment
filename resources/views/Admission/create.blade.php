@extends('layouts.frontend.master')

@section('title','Admission')

@section('content')


@include('messages.message')

<form action="/admissions" method="POST">
	{{csrf_field()}}
	
	<select name="departments_id">
		<option value=""> Select a Department </option>
		@foreach($departments as $data)
		<option value="{{$data->id}}">{{$data->dpt_name}}</option>
		@endforeach
	</select>

	<input type="text" name="std_name" placeholder="std name">
	<input type="text" name="mobile" placeholder="mobile">

	<textarea name="address" placeholder="address"></textarea>

	<input type="submit" value="Save">
 

</form>

@endsection



