@extends('layouts.frontend.master')
@section('title','Login')

@section('content')

<br>
<br>
<br>
<br>

 
<div class="container">
	@include('messages.message')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
  <h2>Login form</h2>
  <form action="/loginUser" method="POST">
  	{{csrf_field()}}
   
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
    </div>
      
 
    <button type="submit" class="btn btn-default">Login</button>
  </form>

</div></div>
</div>


<br>

@endsection