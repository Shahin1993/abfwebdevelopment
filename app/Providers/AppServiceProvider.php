<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Departments;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view::composer('*',function($view){
          
           $departments=Departments::all();
           $view->with('departments',$departments);

       });
    }
}
