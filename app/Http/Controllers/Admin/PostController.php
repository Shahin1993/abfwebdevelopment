<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Posts;
use Image;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category=Categories::all();
        $posts=Posts::orderBy('id','desc')->get();
        return view('Admin.post.view',compact('category','posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
    'categories_id' => 'required',
    'title' => 'required|max:255|min:3',
    'description' => 'required',
    'image' => 'required',
]);

          $std=new Posts;

           if($request->hasfile('image'))
      {  
        $image=$request->file('image');

        $file_name=time().'.'.$image->getClientOriginalExtension();

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1000,600);

        $image_resize->save('images/'.$file_name);
            $std->image= $file_name;
      }

        $std->title=$request->title;
        $std->description=$request->description;
        $std->categories_id=$request->categories_id;
      
        $std->save();


       return back()->with('success','Data inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
