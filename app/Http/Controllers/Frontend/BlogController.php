<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Posts;
class BlogController extends Controller
{
    public function view()
    {
    	$blogs=Posts::orderBy('id','desc')->paginate(5);
    	return view('frontend.blog.view',compact('blogs'));
    }
    public function details($id)
    {
    	$data=Posts::find($id);
        $data->view=$data->view + 1;
        $data->save();

    	return view('frontend.blog.details',compact('data'));
    }
}
