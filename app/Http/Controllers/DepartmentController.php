<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class DepartmentController extends Controller
{


public function __construct()
{
    $this->middleware('auth');
}

  public function index()
  {
     $department=Departments::orderBy('id','desc')->paginate(5);
    return view('Department.view',compact('department'));
  }


    public function create()
    {

        $department=Departments::orderBy('id','desc')->paginate(5);
        return view('Department.shahin.shahin',compact('department'));
    }

    public function store(Request $shahin)
    {

        $shahin->validate([
    'dpt_name' => 'required|unique:departments|max:255|min:3',
    'dpt_code' => 'required',
]);
        
       $data=$shahin->all();
       Departments::create($data);
       return back()->with('success','Data inserted successfully');
    }





    public function destroy($id)
    {
       $data=Departments::find($id);
       $data->delete();
       return back()->with('success','Delete this data');
    }


    public function edit($id)
    {
      $data=Departments::find($id);
      return view('Department.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {

       $data=Departments::find($id);
       $new_data=$request->all();
       $data->update($new_data);
       return redirect('/departments/create')->with('success','Update Successful');

    }
}
