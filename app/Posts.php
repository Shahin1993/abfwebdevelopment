<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
	protected $fillable=['title','description','image','categories_id'];
	
    public function categories()
    {
        	return $this->belongsTo('App\Categories');
    }
    
     public function comments()
    {
        	return $this->hasMany('App\Comments');
    }
}
